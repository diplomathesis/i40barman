//
// Created by magino on 12. 3. 2020.
//

#ifndef FROMBYTES_H
#define FROMBYTES_H

namespace FromBytes {
    static void ToUInt16(uint8_t hiByte, uint8_t loByte, uint16_t& out) {
        out = (hiByte << 8) | loByte;
    }

    static void ToUInt64(std::vector<uint8_t>& inputBytes, uint64_t& out) {
        for (int i=0; i<8; ++i)
            out = (out << 8) | inputBytes[i];
    }

    static void ToFloat(std::vector<uint8_t>& inputBytes, float& out) {
        uint8_t b[] = {inputBytes[0], inputBytes[1], inputBytes[2], inputBytes[3]};
        memcpy(&out, &b, sizeof(out));
    }
};

#endif //FROMBYTES_H