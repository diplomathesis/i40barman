//
// Created by magino on 17. 3. 2020.
//

#ifndef VECTORHELP_H
#define VECTORHELP_H

namespace VectorHelp {
    static std::vector<uint8_t> CreateSubVector(std::vector<uint8_t>& rawData, int indexStart, int indexEnd) {
        std::vector<uint8_t> subVector(rawData.begin()+indexStart, rawData.begin()+indexEnd);
        return subVector;
    }
};

#endif //VECTORHELP_H