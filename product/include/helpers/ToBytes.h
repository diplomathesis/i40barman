//
// Created by magino on 12. 3. 2020.
//

#ifndef TOBYTES_H
#define TOBYTES_H

namespace ToBytes {
    static void Split16bitsToBytes(std::vector<uint8_t>& data, uint16_t number) {
        data.push_back(number >> 8);
        data.push_back(number);
    }

    static void Split32BitsToBytes(std::vector<uint8_t>& data, uint32_t number) {
        data.push_back(number >> 24);
        data.push_back(number >> 16);
        data.push_back(number >> 8);
        data.push_back(number);
    }

    static void Split64BitsToBytes(std::vector<uint8_t>& data, uint64_t number) {
        uint8_t *p = (uint8_t *)&number;
        for(int i = 0; i < 8; i++)
            data.push_back(p[i]);
    }

    static void SplitStringToBytes(std::vector<uint8_t>& data, std::string str, int size) {
        std::vector<uint8_t> convData(str.begin(), str.end());
        data.insert(data.end(), convData.begin(), convData.end());
        for(int i=0; i < (size - str.size()); ++i)
            data.push_back('\0');
    }

    static void SplitFloatToBytes(std::vector<uint8_t>& data, float number) {
        uint8_t* amount = reinterpret_cast<uint8_t*>(&number);
        data.emplace_back(amount[0]);
        data.emplace_back(amount[1]);
        data.emplace_back(amount[2]);
        data.emplace_back(amount[3]);
    }
};

#endif //TOBYTES_H