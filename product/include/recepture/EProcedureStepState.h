//
// Created by magino on 2. 3. 2020.
//

#ifndef EPROCEDURESTEPSTATUS_H
#define EPROCEDURESTEPSTATUS_H

enum class EProcedureStepState: uint16_t {
    NotProcessed = 0,
    ReservedAction = 10,
    ReservedTransport = 20,
    Transporting = 30,
    InProgress = 40,
    Done=255
};

inline std::ostream& operator<<(std::ostream& stream, EProcedureStepState const& value) {
    switch(value) {
        case EProcedureStepState::NotProcessed:
            stream << "Not Processed" << std::endl;
            break;
        case EProcedureStepState::ReservedAction:
            stream << "Action Reserved" << std::endl;
            break;
        case EProcedureStepState::ReservedTransport:
            stream << "Transport Reserved" << std::endl;
            break;
        case EProcedureStepState::Transporting:
            stream << "Transporting" << std::endl;
            break;
        case EProcedureStepState::InProgress:
            stream << "In Progress" << std::endl;
            break;
        case EProcedureStepState::Done:
            stream << "Done" << std::endl;
            break;
    }
}

#endif //EPROCEDURESTEPSTATUS_H