//
// Created by magino on 1. 3. 2020.
//

#ifndef PIECEITEM_H
#define PIECEITEM_H

#include "helpers/VectorHelp.h"
#include "helpers/FromBytes.h"

class PieceItem {
private:
public:
    uint8_t classa;
    uint8_t definition;
    float amount;

    PieceItem() = default;
    ~PieceItem() = default;

    PieceItem(std::vector<uint8_t> data) {
        classa = data[0];
        definition = data[1];
        auto subvector = VectorHelp::CreateSubVector(data,2,6);
        FromBytes::ToFloat(subvector, amount);
    }
};

#endif //PIECEITEM_H