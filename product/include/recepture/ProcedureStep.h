//
// Created by magino on 28. 8. 2019.
//

#ifndef PROCEDURESTEP_H
#define PROCEDURESTEP_H

#include <iostream>
#include <utility>
#include "recepture/EProcedureStepState.h"
#include "PieceItem.h"

class ProcedureStep {
public:
    uint8_t order;
    uint8_t operationId;
    float parameterA;
    float parameterB;

    static ProcedureStep GetTransportStep() {
        return ProcedureStep(0, 3, 0.0, 0.0);
    }

    ProcedureStep() = default;
    ~ProcedureStep() = default;
    ProcedureStep(uint8_t order, uint8_t operationId, float parA, float parB)
        : order(order),
          operationId(operationId),
          parameterA(parA),
          parameterB(parB) {}

    ProcedureStep(std::vector<uint8_t> data) {
        order = data[0];
        operationId = data[1];
        auto vec = VectorHelp::CreateSubVector(data,2,6);
        FromBytes::ToFloat(vec, parameterA);

        vec = VectorHelp::CreateSubVector(data,6,10);
        FromBytes::ToFloat(vec, parameterB);
    }
};

#endif //PROCEDURESTEP_H