//
// Created by magino on 30.4.19.
//

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <fstream>
#include <nlohmann/json.hpp>
#include "configuration/MqttCfg.h"

class Configuration {
private:
    nlohmann::json _json;
public:
    MqttCfg mqttCfg;

    Configuration() = default;
    ~Configuration() = default;

    void Load(std::string fileName) {
        std::ifstream ifs{fileName};
        ifs >> _json;
        mqttCfg = _json["Mqtt"];
    }
};

#endif //CONFIGURATION_H