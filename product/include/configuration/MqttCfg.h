//
// Created by magino on 11. 3. 2020.
//

#ifndef MQTTCFG_H
#define MQTTCFG_H

#include <nlohmann/json.hpp>

class MqttCfg {
public:
    std::string brokerIpAddress;
    std::string clientId;
    std::string rfidReadDataTopic;
    std::string rfidWriteDataTopic;
    int qos;

    MqttCfg() = default;
    ~MqttCfg() = default;

    MqttCfg(const nlohmann::json& j) {
        brokerIpAddress = j["BrokerIpAddress"];
        clientId = j["ClientId"];
        rfidReadDataTopic = j["RfidReadDataTopic"];
        rfidWriteDataTopic = j["RfidWriteDataTopic"];
        qos = j["QOS"];
    }
};

#endif //MQTTCFG_H