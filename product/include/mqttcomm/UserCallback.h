//
// Created by magino on 11. 3. 2020.
//

#ifndef USERCALLBACK_H
#define USERCALLBACK_H

class UserCallback: public virtual mqtt::callback
{
    void connection_lost(const std::string& cause) override {
        std::cout << "\nConnection lost" << std::endl;
        if (!cause.empty())
            std::cout << "\tcause: " << cause << std::endl;
    }

    void delivery_complete(mqtt::delivery_token_ptr tok) override {
        std::cout << "\n[Delivery complete for token: " << (tok ? tok->get_message_id() : -1) << "]" << std::endl;
    }
};
#endif //USERCALLBACK_H
