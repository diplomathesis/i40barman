//
// Created by magino on 12. 3. 2020.
//

#ifndef WRITEDATAMODEL_H
#define WRITEDATAMODEL_H

#include <nlohmann/json.hpp>

struct WriteSectorDataModel {
public:
    std::vector<uint8_t> data;
    uint8_t sector;

    WriteSectorDataModel() = default;
    ~WriteSectorDataModel() = default;
    WriteSectorDataModel(std::vector<uint8_t>& data, uint8_t userSectorIndex): data(data), sector(userSectorIndex) {}
};

static void to_json(nlohmann::json& j, WriteSectorDataModel const& wf) {
    j["data"] = wf.data;
    j["sector"] = wf.sector;
}

#endif //WRITEDATAMODEL_H