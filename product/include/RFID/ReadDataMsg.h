//
// Created by magino on 25. 2. 2020.
//

#ifndef READDATAMSG_H
#define READDATAMSG_H

#include <helpers/VectorHelp.h>

class ReadDataMsg {
private:
public:
    nlohmann::json originalJson;

    std::string tagUid;
    std::string tagVendor;
    std::string tagType;
    uint8_t tagProtocol;

    int dataSectorStartIndex;
    int userDataSize;
    int dataSectorCount;

    std::vector<uint8_t> rawData;

    std::string readState;
    uint64_t timestamp;

    ReadDataMsg(const nlohmann::json& json) {
        originalJson = json;

        auto json_tag = originalJson["tag"];
        json_tag.at("tag_vendor").get_to(tagVendor);
        json_tag.at("tag_type").get_to(tagType);
        json_tag.at("tag_protocol").get_to(tagProtocol);
        json_tag.at("user_memory_offset").get_to(dataSectorStartIndex);
        json_tag.at("tag_size").get_to(userDataSize);
        dataSectorCount = userDataSize/4;

        auto json_data = originalJson["data"];
        rawData.reserve(json_data.size() * 4);

        // Parsing array of arrays into vector of bytes (unsigned char)
        for (auto const& subArray: json_data)
            for (auto const& element: subArray)
                rawData.emplace_back(element);

        auto uid = originalJson["uid"].get<std::vector<unsigned char>>();
        tagUid = std::string(uid.begin(), uid.end());

        originalJson.at("read_state").get_to(readState);

        float time;
        originalJson.at("timestamp").get_to(time);
        timestamp = (uint64_t)time;
    }

    std::vector<uint8_t> GetUserRawData() {
        return VectorHelp::CreateSubVector(rawData, 4*4, rawData.size());
    }
};

#endif //READDATAMSG_H