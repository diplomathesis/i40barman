//
// Created by magino on 1. 2. 2020.
//

#ifndef UAEXPANDEDNODEID_H
#define UAEXPANDEDNODEID_H

#include <open62541/types.h>
#include <open62541/nodeids.h>

// TODO refactor with templates?? also UaNodeId and UaVariant
class UaExpandedNodeId {
private:
    UA_ExpandedNodeId _expandedNodeId;
public:
    UaExpandedNodeId() {
        _expandedNodeId = UA_EXPANDEDNODEID_NULL;
    }
    UaExpandedNodeId(const UA_ExpandedNodeId& expNodeId) {
        _expandedNodeId = expNodeId;
    }
    UaExpandedNodeId(const unsigned short nsIndex, const unsigned id) {
        _expandedNodeId = UA_EXPANDEDNODEID_NUMERIC(nsIndex, id);
    }

    const UA_ExpandedNodeId& GetExpNodeId() const {
        return _expandedNodeId;
    }

    static const UaExpandedNodeId ModellingRuleMandatory;
};

const UaExpandedNodeId UaExpandedNodeId::ModellingRuleMandatory(0, UA_NS0ID_MODELLINGRULE_MANDATORY);

#endif //UAEXPANDEDNODEID_H