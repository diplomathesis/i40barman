//
// Created by magino on 29. 10. 2019.
//

#ifndef UAVARIANT_H
#define UAVARIANT_H

#include <open62541/types.h>

class UaVariant {
private:
    UA_Variant _variant;
public:
    UA_Variant& GetVariant() { return _variant; }
    UA_Variant* GetVariantRef() { return &_variant; }

    template<typename T>
    T GetVariantValue() {
        if (!UA_Variant_isEmpty(&_variant) && _variant.arrayDimensionsSize == 0) {
            //return *((T *)variant.data); // cast to a value - to do Type checking needed
            return *static_cast<T*>(_variant.data);
        }
        return T();
    }
};

#endif //UAVARIANT_H