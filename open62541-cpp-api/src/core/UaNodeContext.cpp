//
// Created by magino on 5. 10. 2019.
//

#include "core/UaNodeContext.h"

UA_DataSource UaNodeContext::_dataSource =
{
        UaNodeContext::ReadDataSource,
        UaNodeContext::WriteDataSource
};

UA_ValueCallback UaNodeContext::_valueCallback =
{
        UaNodeContext::ReadValueCallback,
        UaNodeContext::WriteValueCallback
};

UA_NodeTypeLifecycle UaNodeContext::_nodeTypeLifeCycle =
{
        UaNodeContext::TypeConstructor,
        UaNodeContext::TypeDestructor
};

UA_MethodCallback UaNodeContext::_methodCallback =
{
        UaNodeContext::_methodCallback
};

int UaNodeContext::UNIQUE_NUMERIC_NODEID_IN_APP_NAMESPACE = 1;