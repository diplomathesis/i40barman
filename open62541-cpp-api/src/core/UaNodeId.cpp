//
// Created by magino on 6. 10. 2019.
//

#include "core/UaNodeId.h"

UaNodeId UaNodeId::ObjectsFolder(0, UA_NS0ID_OBJECTSFOLDER);
UaNodeId UaNodeId::Server(0, UA_NS0ID_SERVER);
UaNodeId UaNodeId::Null(0,0);

UaNodeId UaNodeId::BaseVariableType(0, UA_NS0ID_BASEVARIABLETYPE);
UaNodeId UaNodeId::BaseDataVariableType(0, UA_NS0ID_BASEDATAVARIABLETYPE);
UaNodeId UaNodeId::PropertyType(0, UA_NS0ID_PROPERTYTYPE);
UaNodeId UaNodeId::BaseObjectType(0, UA_NS0ID_BASEOBJECTTYPE);
UaNodeId UaNodeId::FolderType(0, UA_NS0ID_FOLDERTYPE);

UaNodeId UaNodeId::HasComponent(0, UA_NS0ID_HASCOMPONENT);
UaNodeId UaNodeId::HasSubType(0, UA_NS0ID_HASSUBTYPE);
UaNodeId UaNodeId::HasProperty(0, UA_NS0ID_HASPROPERTY);
UaNodeId UaNodeId::Organizes(0, UA_NS0ID_ORGANIZES);
UaNodeId UaNodeId::HasOrderedComponent(0, UA_NS0ID_HASORDEREDCOMPONENT);
UaNodeId UaNodeId::HasModellingRule(0, UA_NS0ID_HASMODELLINGRULE);