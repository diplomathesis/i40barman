//
// Created by magino on 30.4.19.
//

#include <configuration/Configuration.h>
#include <Constants.h>

Configuration::Configuration() {
    std::ifstream ifs(Constants::CFG_FILE_NAME);
    ifs >> _json;
}
UaServerCfg Configuration::GetServerConfig() {
    UaServerCfg cfg = _json["UaServer"];
    return cfg;
}
CommunicationCfg Configuration::GetCommunicationConfig() {
    CommunicationCfg cfg = _json["Communication"];
    return cfg;
}