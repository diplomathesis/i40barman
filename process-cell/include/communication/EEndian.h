//
// Created by magino on 22. 3. 2020.
//

#ifndef EENDIAN_H
#define EENDIAN_H

enum class EEndian {
    BigEndian,
    LittleEndian
};

#endif //EENDIAN_H