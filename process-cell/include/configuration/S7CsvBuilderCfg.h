//
// Created by magino on 28. 5. 2020.
//

#ifndef S7CSVBUILDERCFG_H
#define S7CSVBUILDERCFG_H

#include <string>
#include <nlohmann/json.hpp>

class S7CsvBuilder {
public:
    std::string csvFileName;

    S7CsvBuilder() = default;
    ~S7CsvBuilder() = default;
    S7CsvBuilder(const nlohmann::json& j) {
        csvFileName = j["CsvFileName"];
    }
};

#endif //S7CSVBUILDERCFG_H