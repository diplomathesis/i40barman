//
// Created by magino on 20.4.19.
//

#ifndef UASERVERCFG_H
#define UASERVERCFG_H

#include <string>
#include <nlohmann/json.hpp>

class UaServerCfg {
public:
    unsigned short int port;
    std::string appName;
    std::string appUri;
    std::string productUri;
    std::string mDnsName;
    std::string ipAddress;
    std::string namespaceName;
    std::vector<std::string> capabilities;

    UaServerCfg() = default;
    ~UaServerCfg() = default;

    UaServerCfg(const nlohmann::json& j) {
        port = j["Port"];
        appName = j["AppName"];
        appUri = j["AppUri"];
        productUri = j["ProductUri"];
        mDnsName = j["mDnsName"];
        ipAddress = j["IpAddress"];
        namespaceName = j["Namespace"];

        auto jsonCapabilitiesArray = j["Capabilities"];
        capabilities.reserve(jsonCapabilitiesArray.size());

        for (auto const& item: jsonCapabilitiesArray)
            capabilities.emplace_back(item);
    }
};

#endif //UASERVERCFG_H