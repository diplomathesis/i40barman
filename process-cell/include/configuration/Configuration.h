//
// Created by magino on 30.4.19.
//

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <fstream>
#include "configuration/UaServerCfg.h"
#include "configuration/ModbusTcpClientCfg.h"
#include "CommunicationCfg.h"
#include <nlohmann/json.hpp>

class Configuration {
private:
    nlohmann::json _json;
public:
    Configuration();
    ~Configuration() = default;
    UaServerCfg GetServerConfig();
    CommunicationCfg GetCommunicationConfig();
};

#endif //CONFIGURATION_H