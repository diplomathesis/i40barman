//
// Created by magino on 20.5.2019.
//

#ifndef MODBUSTCPCLIENTCFG_H
#define MODBUSTCPCLIENTCFG_H

#include <string>
#include <nlohmann/json.hpp>

class ModbusTcpClientCfg {
public:
    std::string ipAddress;
    uint16_t port;
    unsigned int cycleTime;
    unsigned int minAddress;
    unsigned int maxAddress;

    ModbusTcpClientCfg() = default;
    ~ModbusTcpClientCfg() = default;

    ModbusTcpClientCfg(const nlohmann::json& j) {
        ipAddress = j["IpAddress"];
        port = j["Port"];
        cycleTime = j["CycleTime"];
        minAddress = j["MinAddress"];
        maxAddress = j["MaxAddress"];
    }
};

#endif //MODBUSTCPCLIENTCFG_H