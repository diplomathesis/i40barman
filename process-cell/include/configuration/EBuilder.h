//
// Created by magino on 28. 5. 2020.
//

#ifndef EBUILDER_H
#define EBUILDER_H

enum class EBuilder {
    None = 0,
    S7Csv = 1,
    // If we implement new builder we can add definition here
};

#endif //EBUILDER_H