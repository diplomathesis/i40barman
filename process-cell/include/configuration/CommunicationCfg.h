//
// Created by magino on 28. 5. 2020.
//

#ifndef COMMUNICATIONCFG_H
#define COMMUNICATIONCFG_H

#include <nlohmann/json.hpp>
#include <open62541/plugin/log_stdout.h>
#include <open62541/plugin/log.h>
#include "ModbusTcpClientCfg.h"
#include "ECommunication.h"
#include "EBuilder.h"
#include "S7CsvBuilderCfg.h"

class CommunicationCfg {
public:
    ECommunication commType;
    EBuilder builderType;

    ModbusTcpClientCfg modbusClientCfg;
    S7CsvBuilder s7CsvBuilder;

    CommunicationCfg(const nlohmann::json& j) {
        std::string commtype = j["CommType"];
        std::string buildertype = j["BuilderType"];

        if(commtype == "ModbusTcp")
            commType = ECommunication::ModbusTcp;
        //elseif(commtype == "OtherCommType")
            // commType = ECommunication::Other;
        else {
            UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Unknown communication type!");
            commType = ECommunication::None;
        }

        if(buildertype == "S7Csv")
            builderType = EBuilder::S7Csv;
        //elseif(buildertype == "OtherBuilder")
            // builderType = EBuilder::OtherBuilder;
        else {
            UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Unknown builder type!");
            builderType = EBuilder::None;
        }
        switch (commType) {
            case ECommunication::ModbusTcp:
                modbusClientCfg = j["ModbusTcp"];
                break;
            //case ECommunication::OtherCommType:
            //    modbusClientCfg = j["OtherCommType"];
            //    break;
            //default:
        }
        switch (builderType) {
            case EBuilder::S7Csv:
                s7CsvBuilder = j["S7CsvBuilder"];
                break;
            //case EBuilder::OtherBuilder:
            //    otherBuilderCfg = j["OtherBuilder"];
            //    break;
            //default:
        }
    }

    ModbusTcpClientCfg GetModbusTcpConfig() {
        return modbusClientCfg;
    }

    S7CsvBuilder GetS7CsvBuilderConfig() {
        return s7CsvBuilder;
    }
};

#endif //COMMUNICATIONCFG_H