//
// Created by magino on 28. 5. 2020.
//

#ifndef ECOMMUNICATION_H
#define ECOMMUNICATION_H

enum class ECommunication {
    None = 0,
    ModbusTcp = 1,
    // If we implement new communication we can add definition here
};

#endif //ECOMMUNICATION_H