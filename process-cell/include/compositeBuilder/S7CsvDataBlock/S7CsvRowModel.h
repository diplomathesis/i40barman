//
// Created by magino on 2.6.19.
//

#ifndef S7CSVROWMODEL_H
#define S7CSVROWMODEL_H

#include <string>
#include <iostream>
#include <cmath>
#include <open62541/types.h>
#include <open62541/types_generated.h>
#include "compositeBuilder/SiemensDataType.h"

class S7CsvRowModel {
private:
    UA_NodeClass ParseNodeType(const std::string& comment) {
        const char type = comment.front();
        switch(type) {
            case 'o': return UA_NODECLASS_OBJECT;
            case 'm': return UA_NODECLASS_METHOD;
            case 'p': return UA_NODECLASS_VARIABLE;
            default: UA_NODECLASS_UNSPECIFIED;
        }
    }
public:
    std::string name;
    SiemensDataType dataType;
    UA_DataType uaDataType;
    double offset;
    int address;
    int bit;
    bool accessible;
    bool writable;
    bool visible;
    UA_NodeClass nodeType;
    std::string comment;
    int level;
    std::string description;

    S7CsvRowModel() = default;
    S7CsvRowModel(const std::string& name, const std::string& comment):
    name(name),
    comment(comment),
    level(std::stoi(comment.substr(1, 2))) {
        if (comment.size() > 2)
            description = comment.substr(3, comment.size());
    }

    S7CsvRowModel(const std::string& name,
                  const SiemensDataType dataType,
                  const UA_DataType uaDataType,
                  const double offset,
                  const std::string& accessible,
                  const std::string& writable,
                  const std::string& visible,
                  const std::string& comment):
                    name(name),
                    dataType(dataType),
                    offset(offset),
                    accessible(accessible == "True" || accessible == "TRUE"),
                    writable(writable == "True" || writable == "TRUE"),
                    visible(visible == "True" || visible == "TRUE"),
                    comment(comment),
                    nodeType(ParseNodeType(comment)),
                    uaDataType(uaDataType),
                    level(std::stoi(comment.substr(1, 2)))
                    {
        if(comment.size()>2)
            description = comment.substr(3, comment.size());

        address = (int)offset;
        float tmp = (offset - address)*10;
        bit = rint(tmp);
    }

    virtual ~S7CsvRowModel() = default;
    bool IsObject() const { return comment.find('o') != std::string::npos; }
};
#endif //S7CSVROWMODEL_H