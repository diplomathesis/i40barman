//
// Created by magino on 27.5.19.
//

#ifndef SIEMENSDATATYPE_H
#define SIEMENSDATATYPE_H

// All data types for S7-300/400, S7-1200 and S7-1500.
// Programming_guideline_DOKU_v12_en.pdf, 03/2014, page 69-70
enum class SiemensDataType {
    // Bit data types
    BOOL = 0,
    BYTE,
    WORD,
    DWORD,
    LWORD,
    // Character type
    CHAR,
    // Numerical data types
    INT,
    DINT,
    REAL,
    SINT,
    USINT,
    UINT,
    UDINT,
    LREAL,
    LINT,
    ULINT,
    // Time types
    TIME,
    DATE,
    TIME_OF_DAY,
    S5TIME,
    LTIME,
    L_TIME_OF_DAY,
    // Data groups that are made up of other data types
    // Time types
    DT, // DATE_AND_TIME
    DTL,
    LDT, // L_DATE_AND_TIME
    // Character type
    STRING, // String[10]
            // -> real data length is 12 bytes
            // -> 1.byte = Total length of string
            // -> 2.byte = actual (real) string length
            // -> this two bytes are followed by 10bytes of data
            // example: String[5] = "Car" => 1.byte = 5, 2.byte = 3, 5 Bytes Data[C,a,r,null,null] or not allocated, not sure
    // Field
    ARRAY,
    // Structure
    STRUCT,
    // Pointer
    POINTER,
    ANY,
    VARIANT,
    // Blocks
    TIMER,
    COUNTER,
    BLOCK_FB,
    BLOCK_FC,
    BLOCK_DB,
    BLOCK_SDB,
    VOID,
    // User defined data type
    UDT,
};

#endif //SIEMENSDATATYPE_H