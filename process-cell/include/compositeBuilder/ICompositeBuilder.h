//
// Created by magino on 6.6.19.
//

#ifndef ICOMPOSITEBUILDER_H
#define ICOMPOSITEBUILDER_H

#include <composite/INode.h>
#include <Management.h>

class ICompositeBuilder {
protected:
    ICommunication& comm;
    MyUaServer& uaServer;
    Management& mngmnt;

    INode* rootNode;
public:
    ICompositeBuilder(ICommunication& comm, MyUaServer& server, Management& mngmnt): comm(comm), uaServer(server), mngmnt(mngmnt) {}
    virtual ~ICompositeBuilder() = default;

    virtual ICompositeBuilder& BuildRoot() = 0;
    virtual ICompositeBuilder& BuildTree() = 0;

    INode* GetRootNode() { return rootNode; }
};

#endif //ICOMPOSITEBUILDER_H