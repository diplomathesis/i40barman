//
// Created by magino on 26.5.19.
//

#ifndef INODE_H
#define INODE_H

#include <string>
#include <open62541/types.h>
#include <MyUaServer.h>
#include <core/UaNodeContext.h>
#include "INode.h"

class INode: public UaNodeContext {
private:
protected:
    ICommunication& comm;
    MyUaServer& uaServer;
public:
    INode(ICommunication& comm, MyUaServer& uaServer)
        : comm(comm),
          uaServer(uaServer),
          UaNodeContext(uaServer.GetAppNsIndex()) {}
    ~INode() override = default;

    virtual void Add(INode* node) {};
    virtual void AddOpcUaNode(UaNodeId& parent) = 0;
    //virtual IS7Node* FindNode(const UaNodeId& uaNodeId) = 0;
    virtual void* GetCommParam() = 0;
    virtual UA_DataType GetUaDataType() const = 0;

    UaNodeId& GetNodeIdd() { return instanceNodeId; };
};

#endif //INODE_H