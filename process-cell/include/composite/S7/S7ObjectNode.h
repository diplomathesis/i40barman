//
// Created by magino on 27.5.19.
//

#ifndef S7OBJECTNODE_H
#define S7OBJECTNODE_H

#include <string>
#include <list>
#include <vector>

#include "core/UaObjects.h"

#include <communication/ICommunication.h>
#include <compositeBuilder/S7CsvDataBlock/S7CsvRowModel.h>
#include "composite/INode.h"


class S7ObjectNode: public INode {
private:
    std::list<INode*> _nodes;
    S7CsvRowModel _row;
public:
    S7ObjectNode(ICommunication& comm, MyUaServer& server, S7CsvRowModel& row): INode(comm, server), _row(row) {}
    ~S7ObjectNode() override = default;

    void* GetCommParam() override { return &_row; }
    UA_DataType GetUaDataType() const override  { return _row.uaDataType; };

    void Add(INode* node) override {
        _nodes.push_back(node);
    }
    void AddOpcUaNode(UaNodeId& parent) override {
        //Work around for not include first object [Desperate times call for desperate measures]
        if (_row.level != 0) {
            UaNodeId tmpNodeId = uaServer.AddObjectNode(_row.name,
                                                        _row.name,
                                                        _row.description,
                                                        false,
                                                        parent,
                                                        instanceNodeId,
                                                        UaNodeId::HasComponent,
                                                        UaNodeId::BaseObjectType,
                                                        this);
            // Build all children
            for (auto &node: _nodes) {
                node->AddOpcUaNode(instanceNodeId);
            }
        } else {
            // Build all children
            for (auto &node: _nodes) {
                node->AddOpcUaNode(parent);
            }
        }
    }

    /*
    IS7Node* FindNode(const UaNodeId& uaNodeId) override {
        //if(UA_NodeId_equal(&nodeId, uaNodeId))
        // return this;
        if(nodeIdd == uaNodeId)
            return this;
        else {
            for (auto &node : nodes) {
                IS7Node* tmpNode = node.FindNode(uaNodeId);
                if(tmpNode) return tmpNode;
            }
        }
        return nullptr;
    }
    */
};

#endif //S7OBJECTNODE_H