//
// Created by magino on 6.6.19.
//

#ifndef NODETREE_H
#define NODETREE_H

#include <memory>
#include <communication/ICommunication.h>
#include "composite/INode.h"
#include <open62541/nodeids.h>

class NodeTree {
private:
    INode& _root;
public:
    NodeTree(INode& root): _root(root) {}

    void BuildOpcUaAddressSpace(UaNodeId& parent) const {
        _root.AddOpcUaNode(parent);
    }

    /*IS7Node* FindNode(const UaNodeId& nodeId) const {
        return root.FindNode(nodeId);
    }*/
};

#endif //NODETREE_H