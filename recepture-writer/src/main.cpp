//
// Created by magino on 2. 3. 2020.
//

// ultility project for parsing human readable recepture from json format into byte json format for rfid write
#include <nlohmann/json.hpp>
#include <ReceptureJson.h>
#include <MqttPublisher.h>

int main() {
    std::string fileName = "recepture_notprocessed.json";
    //std::string fileName = "recepture_transporting.json";
    //std::string fileName = "recepture_inprogress.json";

    ReceptureJson recipeJson;
    recipeJson.LoadJson(fileName)
              .ConnectMqtt()
              .ParseData()
              .CreateMqttMsg()
              .PublishReceptureMsgMultiSector()
              .DisconnectMqtt();

    std::cout << std::endl;
    std::cout << recipeJson.inputJson.dump() << std::endl;
    std::cout << recipeJson.outputJson.dump() << std::endl;
    std::cout << std::endl;

    std::cout << "Exiting" << std::endl;
    return 0;
}