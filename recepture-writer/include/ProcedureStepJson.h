//
// Created by magino on 5. 3. 2020.
//

#ifndef PROCEDURESTEP_H
#define PROCEDURESTEP_H

#include <cstdint>
#include <nlohmann/json.hpp>

class ProcedureStepJson {
public:
    uint8_t order;
    uint8_t operation;
    float parA;
    float parB;

    ProcedureStepJson() = default;
    ~ProcedureStepJson() = default;

    ProcedureStepJson(const nlohmann::json& j) {
        order = j["Order"];
        operation = j["Operation"];
        parA = j["ParA"];
        parB = j["ParB"];
    }
};

#endif //PROCEDURESTEP_H