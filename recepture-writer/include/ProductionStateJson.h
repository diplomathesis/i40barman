//
// Created by magino on 9. 3. 2020.
//

#ifndef PRODUCTIONSTATEJSON_H
#define PRODUCTIONSTATEJSON_H

class ProductionStateJson {
public:
    uint16_t state;
    uint16_t step;
    uint64_t reservedActionCellId;
    uint64_t actionReservationId;
    uint64_t reservedTransportCellId;
    uint64_t transportReservationId;

    ProductionStateJson() = default;
    ~ProductionStateJson() = default;

    ProductionStateJson(const nlohmann::json& j) {
        state = j["State"];
        step = j["Step"];
        reservedActionCellId = j["ReservedActionCellId"];
        actionReservationId = j["ActionReservationId"];
        reservedTransportCellId = j["ReservedTransportCellId"];
        transportReservationId = j["TransportReservationId"];
    }
};

#endif //PRODUCTIONSTATEJSON_H