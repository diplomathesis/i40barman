//
// Created by magino on 5. 3. 2020.
//

#ifndef HEADERJSON_H
#define HEADERJSON_H

#include <cstdint>
#include <nlohmann/json.hpp>

class HeaderJson {
public:
    uint16_t recipeId;
    uint16_t recipeVersion;
    uint64_t releaseDateTime;
    uint64_t serialNumber;
    uint64_t orderDateTime;
    uint64_t dispenseDateTime;
    uint16_t status;
    uint8_t pieceListLength;
    uint8_t procedureLength;

    HeaderJson() = default;
    ~HeaderJson() = default;

    HeaderJson(const nlohmann::json& j) {
        recipeId = j["RecipeId"];
        recipeVersion = j["RecipeVersion"];
        releaseDateTime = j["ReleaseDateTime"];
        serialNumber = j["SerialNumber"];
        orderDateTime = j["OrderDateTime"];
        dispenseDateTime = j["DispenseDateTime"];
        status = j["Status"];
        pieceListLength = j["PieceListLength"];
        procedureLength = j["ProcedureLength"];
    }
};

#endif //HEADERJSON_H