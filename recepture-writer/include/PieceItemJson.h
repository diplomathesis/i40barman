//
// Created by magino on 5. 3. 2020.
//

#ifndef PIECEITEM_H
#define PIECEITEM_H

class PieceItemJson {
public:
    uint8_t classa;
    uint8_t definition;
    float amount;

    PieceItemJson() = default;
    ~PieceItemJson() = default;

    PieceItemJson(const nlohmann::json& j) {
        classa = j["Class"];
        definition = j["Definition"];
        amount = j["Amount"];
    }
};

#endif //PIECEITEM_H