//
// Created by magino on 8. 3. 2020.
//

#ifndef WRITEFORMAT_H
#define WRITEFORMAT_H

struct WriteFormat {
public:
    std::vector<uint8_t> data;
    uint8_t sector;
};

void to_json(nlohmann::json& j, WriteFormat const& wf) {
    j["data"] = wf.data;
    j["sector"] = wf.sector;
}

#endif //WRITEFORMAT_H